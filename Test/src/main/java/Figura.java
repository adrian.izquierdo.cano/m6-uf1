import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Figura")
@XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.NONE)
@XmlType(propOrder={"id","name","costats", "colors"})

public class Figura {
	private int id;
	private String name;
	private int costats;
	private ArrayList<String> colors = new ArrayList();
	
	public Figura()
	{
		super();
	}

	public Figura(int id, String name, int costats) {
		super();
		this.id = id;
		this.name = name;
		this.costats = costats;
	}

	@XmlAttribute
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	@XmlElement(name = "nom")
	public String getName()
	{
		return name;
	}
	public void setName(String nom)
	{
		this.name = nom;
	}
	@XmlElement
	public int getCostats() {
		return costats;
	}

	public void setCostats(int costats) {
		this.costats = costats;
	}
	@XmlElementWrapper(name="colors")
	@XmlElement(name="color")
	public ArrayList<String> getColors() {
		return colors;
	}

	public void setColors(ArrayList<String> colors) {
		this.colors = colors;
	}
	
	
}



