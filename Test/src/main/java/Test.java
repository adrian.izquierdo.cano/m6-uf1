import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Test {
	public static void main(String[] args) {
		
		try {
			ArrayList<String> colors = new ArrayList();
			File file = new File("figures.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Figura.class);
			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			Figura quadrat = new Figura(1,"Quadrat",4);
			colors.add("Blanc");
			colors.add("Negre");
			quadrat.setColors(colors);
			
			marshallerObj.marshal(quadrat, new FileOutputStream(file));
		} catch (JAXBException e)
		{
			e.printStackTrace();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		
		try
		{
			File file = new File("figures.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Figura.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Figura fig = (Figura) jaxbUnmarshaller.unmarshal(file);
			System.out.println(fig.getId()+" "+fig.getName()+" "+fig.getCostats()+" "+fig.getColors());
		} catch (JAXBException e)
		{
			e.printStackTrace();
		}
	}
}

