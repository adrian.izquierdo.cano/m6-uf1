import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONObject;

public class MainOrdenado {

	public static void main(String[] args) {

		JSONObject obj = new JSONObject();
		Map<String, Object> myLinkedHasMap = new LinkedHashMap<String,Object>();
		
		myLinkedHasMap.put("Id", 1);
		myLinkedHasMap.put("Name","Quadrat");
		myLinkedHasMap.put("Costats",4);
		
		ArrayList<String> list = new ArrayList();
		list.add("Lila");
		list.add("Negre");
		myLinkedHasMap.put("Colors", list);
		obj.put("Figura", myLinkedHasMap);
		
		try (FileWriter file = new FileWriter("Figures.json"))
		{
			file.write(obj.toJSONString());
			file.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		System.out.println(obj.toString());
	}

}
