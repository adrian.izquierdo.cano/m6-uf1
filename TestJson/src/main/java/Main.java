import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Main {

	public static void main(String[] args) {

		JSONObject obj = new JSONObject();
		obj.put("Id", 1);
		obj.put("Name","Quadrat");
		obj.put("Costats", 4);
		
		JSONArray list = new JSONArray();
		list.add("Lila");
		list.add("Negre");
		obj.put("Colors", list);
		
		try (FileWriter file = new FileWriter("Figures.json"))
		{
			file.write(obj.toJSONString());
			file.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		System.out.println(obj.toString());
	}

}
