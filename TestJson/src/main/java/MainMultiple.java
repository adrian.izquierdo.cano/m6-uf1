import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MainMultiple {

	public static void main(String[] args) {

		JSONObject obj = new JSONObject();
		ArrayList<Figura> figures = new ArrayList();
		Figura fig = new Figura(1,"Quadrat",4);
		ArrayList<String> colors = new ArrayList();
		colors.add("Lila");
		colors.add("Gris");
		fig.setColors(colors);
		figures.add(fig);
		fig = null;
		fig = new Figura(2,"Triangle",3);
		colors = null;
		colors = new ArrayList();
		colors.add("Negre");
		colors.add("Verd");
		fig.setColors(colors);
		figures.add(fig);
	
	
	try(FileWriter file = new FileWriter("Figuresv2.json"))
	{
		JSONArray list = new JSONArray();
		for(Figura s : figures) 
		{
		Map<String, Object> myLinkedHasMap = new LinkedHashMap<String,Object>();
		myLinkedHasMap.put("Id",s.getId());
		myLinkedHasMap.put("Name",s.getName());
		myLinkedHasMap.put("Costats",s.getCostats());
		myLinkedHasMap.put("Colors",s.getColors());
		list.add(myLinkedHasMap);
		}
	obj.put("Figura", list);
	
	file.write(obj.toJSONString());
	file.flush();
	} catch (IOException e)
	{
		e.printStackTrace();
	}
	System.out.println(obj.toString());
	}
}
