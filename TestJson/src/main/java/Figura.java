import java.util.ArrayList;


public class Figura {
	private int id;
	private String name;
	private int costats;
	private ArrayList<String> colors = new ArrayList();
	
	public Figura(){
		super();
	}

	public Figura(int id, String name, int costats) {
		super();
		this.id = id;
		this.name = name;
		this.costats = costats;
	}
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}
	public void setName(String nom)
	{
		this.name = nom;
	}
	public int getCostats() {
		return costats;
	}

	public void setCostats(int costats) {
		this.costats = costats;
	}

	public ArrayList<String> getColors() {
		return colors;
	}

	public void setColors(ArrayList<String> colors) {
		this.colors = colors;
	}
}
