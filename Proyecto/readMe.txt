Basicamente el proyecto se divide en dos packages uno para el main y los metodos y otro para todos los mapeos.

Si usas el menu en orden esta preparado para que se pueda ver que todo funciona.
Si quieres modificar el resultado de algun ejercicio, en la clase main se hacen las llamadas a los metodos, simplemente modifica
los parametros del metodo que quieras.

El ejercicio 9 realiza una clase totalmente aleatoria, en el enunciado pide solo alumnos, pero en mi programa he hecho que 
la aula y el profesor tambien sean aleatorios.
El ejercicio 10 muestra los nombres de aula, profesor y alumnos, en el enunciado pide que se muestren todos los datos, pero es 
un poco inecesario mostrar todo todo, por ello solo muestro los nombres para que se vea que funciona. En caso de querer algo sobre
este tema es el metodo toString de la clase Classe.
El resto de ejercicios funcionan tal como lo dice el enunciado.



