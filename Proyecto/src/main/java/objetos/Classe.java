package objetos;

import java.io.Serializable;

import java.util.ArrayList;

import org.json.simple.JSONObject;

public class Classe implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7244896334761456664L;
	private Aula aula;
	private String professor;
	ArrayList<Alumne> alumnes;
	
	

	public Classe(Aula aula, String professor, ArrayList<Alumne> alumnes) {
		super();
		this.aula = aula;
		this.professor = professor;
		this.alumnes = alumnes;
	}

	public Aula getAula() {
		return aula;
	}

	public void setAula(Aula aula) {
		this.aula = aula;
	}

	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	public ArrayList<Alumne> getAlumnes() {
		return alumnes;
	}

	public void setAlumnes(ArrayList<Alumne> alumnes) {
		this.alumnes = alumnes;
	}
	
	public String toString()
	{
		String string = "Aula: " + aula.getNom() + "\nProfessor: " + professor + "\nAlumnes: ";

		for (Alumne alumne : alumnes) 
		{
			string += alumne.getCognoms() + ", " + alumne.getNom() + " || ";
		}
		return string;
	}
	
}
