package objetos;

import java.io.Serializable;
import java.util.ArrayList;

public class Professor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2829133082389289295L;
	private ArrayList<String> professors = new ArrayList<String>();

	
	public Professor() {
		super();
	}

	
	public Professor(ArrayList<String> professors) {
		super();
		this.professors = professors;
	}


	public ArrayList<String> getProfessors() {
		return professors;
	}

	public void setProfessors(ArrayList<String> professors) {
		this.professors = professors;
	}
	
	
	
	
	
}
