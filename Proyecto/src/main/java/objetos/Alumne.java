package objetos;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="alumne")
@XmlAccessorType(javax.xml.bind.annotation.XmlAccessType.NONE)
@XmlType(propOrder={"nom","cognoms","dni","adreca","telefons","mail"})

public class Alumne implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7602766703663736921L;
	private String nom;
	private String cognoms;
	private String dni;
	private String adreca;
	private ArrayList<String> telefons = new ArrayList();
	private String mail;
	
	
	
	public Alumne() {
		super();
	}


	

	public Alumne(String nom, String cognoms, String adreca, String dni, ArrayList<String> telefons, String mail) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.dni = dni;
		this.adreca = adreca;
		this.telefons = telefons;
		this.mail = mail;
	}




	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElement(name = "cognoms")
	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
	
	@XmlElement(name = "adreca")
	public String getAdreca() {
		return adreca;
	}


	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}
	
	@XmlElement(name = "DNI")
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
	@XmlElementWrapper(name="telefons")
	@XmlElement(name="telefon")
	public ArrayList<String> getTelefons() {
		return telefons;
	}

	public void setTelefons(ArrayList<String> telefons) {
		this.telefons = telefons;
	}


	@XmlElement(name = "mail")
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}


	
	
	
}
