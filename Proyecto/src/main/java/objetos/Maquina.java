package objetos;

import java.io.Serializable;

public class Maquina implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3113555347131456480L;
	private String nom;
	private String processador;
	private Boolean grafica;
	
	public Maquina() {
		super();
	}

	public Maquina(String nom, String processador, Boolean grafica) {
		super();
		this.nom = nom;
		this.processador = processador;
		this.grafica = grafica;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getProcessador() {
		return processador;
	}

	public void setProcessador(String processador) {
		this.processador = processador;
	}

	public Boolean getGrafica() {
		return grafica;
	}

	public void setGrafica(Boolean grafica) {
		this.grafica = grafica;
	}
	
	
	
}
