package objetos;

import java.io.Serializable;
import java.util.ArrayList;

public class Aula implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -995672684770780230L;
	private String nom;
	private Long capacitat;
	private boolean aireacondicionat;
	private ArrayList<Maquina> maquines = new ArrayList<Maquina>();
	
	public Aula() {
		super();
	}



	public Aula(String nom, Long capacitat, boolean aireacondicionat, ArrayList<Maquina> maquines) {
		super();
		this.nom = nom;
		this.capacitat = capacitat;
		this.aireacondicionat = aireacondicionat;
		this.maquines = maquines;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public Long getCapacitat() {
		return capacitat;
	}



	public void setCapacitat(Long capacitat) {
		this.capacitat = capacitat;
	}



	public boolean isAireacondicionat() {
		return aireacondicionat;
	}



	public void setAireacondicionat(boolean aireacondicionat) {
		this.aireacondicionat = aireacondicionat;
	}



	public ArrayList<Maquina> getMaquines() {
		return maquines;
	}



	public void setMaquines(ArrayList<Maquina> maquines) {
		this.maquines = maquines;
	}
	
	
	
	
}
