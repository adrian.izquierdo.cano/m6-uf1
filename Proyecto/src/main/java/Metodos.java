import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import objetos.Alumne;
import objetos.Aula;
import objetos.Classe;
import objetos.Institut;
import objetos.Maquina;

public class Metodos {

	public static void afegirProfessor(String professor)
	{
		ArrayList<String> professors = new ArrayList<String>();
		
		try {
			FileReader fr = new FileReader("professors.txt");
			BufferedReader br = new BufferedReader(fr);

			String line;
			while(br.ready()) {
				line = br.readLine();
				professors.add(line);
			}
			professors.add(professor);
			br.close();
			fr.close();
			
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			Collections.sort(professors);
		
		try {
			File f = new File("professors.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for(int i=0; i<professors.size();i++) {
				bw.write(professors.get(i).toString());
				bw.newLine();
			}
			bw.flush();
			bw.close();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void jubilarProfessor(String professor)
	{
		ArrayList<String> professors = new ArrayList();
		
		try {
			FileReader fr = new FileReader("professors.txt");
			BufferedReader br = new BufferedReader(fr);

			String line;
			while(br.ready()) {
				line = br.readLine();
				professors.add(line);
			}
			professors.remove(professor);
			br.close();
			fr.close();
			
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			Collections.sort(professors);
		
		try {
			File f = new File("professors.txt");
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for(int i=0; i<professors.size();i++) {
				bw.write(professors.get(i).toString());
				bw.newLine();
			}
			bw.flush();
			bw.close();
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void afegeixAlumne(Alumne al)
	{
		try
		{
			File file = new File("alumnes.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Institut ins = (Institut) jaxbUnmarshaller.unmarshal(file);
			
			ins.getAlumnes().add(al);
			
			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			
			marshallerObj.marshal(ins, new FileOutputStream(file));
			
		} catch (JAXBException e)
		{
			e.printStackTrace();
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

	}
	public static void afegeixTelefon(String dniAlumne, String telèfon)
	{
		try
		{
			File file = new File("alumnes.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Institut ins = (Institut) jaxbUnmarshaller.unmarshal(file);
			
			ArrayList<Alumne> alumnes = new ArrayList<Alumne>();
			alumnes = ins.getAlumnes();
			
			for (int i = 0; i < alumnes.size(); i++) 
			{
				if(alumnes.get(i).getDni().equals(dniAlumne))
				{
					alumnes.get(i).getTelefons().add(telèfon);
				}
				
			}
			
			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			
			marshallerObj.marshal(ins, new FileOutputStream(file));
			
		} catch (JAXBException e)
		{
			e.printStackTrace();
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	public static void alCarrer(String dniAlumne)
	{
		try
		{
			File file = new File("alumnes.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Institut ins = (Institut) jaxbUnmarshaller.unmarshal(file);
			
			ArrayList<Alumne> alumnes = ins.getAlumnes();
			
			for (int i = 0; i < alumnes.size(); i++) 
			{
				if(alumnes.get(i).getDni().equals(dniAlumne))
				{
					alumnes.remove(i);
				}
				
			}
			
			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			
			marshallerObj.marshal(ins, new FileOutputStream(file));
			
		} catch (JAXBException e)
		{
			e.printStackTrace();
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void comprarMaquina(String nomAula, String nomMaquina, String processador, boolean grafica)
	{
		Boolean yaExisteMaquina = false;
		JSONObject JSONmaquina = new JSONObject();
		JSONmaquina.put("nom", nomMaquina);
		JSONmaquina.put("processador", processador);
		JSONmaquina.put("grafica", grafica);
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("aules.json"));
			JSONArray jsonArray = (JSONArray) obj;
			
            for (int i = 0; i < jsonArray.size(); i++) 
            {
            	JSONObject arrayObject = (JSONObject) jsonArray.get(i);
            	String name = (String) arrayObject.get("nom");
            	if (name.equals(nomAula))
            	{
            		JSONArray maquines = (JSONArray) arrayObject.get("maquines");
            		for(Object s : maquines) 
            		{
            			JSONObject maquinesObject = (JSONObject) s;
            			String nameMaquina = (String) maquinesObject.get("nom");
            			if(nameMaquina.contentEquals(nomMaquina))
            			{
            				yaExisteMaquina = true;
            			}
                    }
            		if (yaExisteMaquina == false)
            		{
                		maquines.add(JSONmaquina);
            		}
            	}  	
            	
			}
            try (FileWriter file = new FileWriter("aules.json")) {

                file.write(jsonArray.toJSONString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
      
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}	
	}
	
	public static void canviaMaquina(String nomMaquina, String nomAula)
	{
		Boolean existeMaquina = false;
		JSONParser parser = new JSONParser();
		JSONObject JSONmaquina = new JSONObject();
		try {
			Object obj = parser.parse(new FileReader("aules.json"));
			JSONArray jsonArray = (JSONArray) obj;
			
            for (int i = 0; i < jsonArray.size(); i++) 
            {
            	JSONObject arrayObject = (JSONObject) jsonArray.get(i);
        		JSONArray maquines = (JSONArray) arrayObject.get("maquines");
        		for (int j = 0; j < maquines.size(); j++) 
        		{
        			JSONObject maquinesObject = (JSONObject) maquines.get(j);
        			String nameMaquina = (String) maquinesObject.get("nom");
        			String nameProcessador = (String) maquinesObject.get("processador");
        			Boolean nameGrafica = (Boolean) maquinesObject.get("grafica");
        			if(nameMaquina.contentEquals(nomMaquina))
        			{
        				existeMaquina = true;
        				JSONmaquina.put("nom", nomMaquina);
        				JSONmaquina.put("processador", nameProcessador);
        				JSONmaquina.put("grafica", nameGrafica);
        				maquines.remove(j);
        			}
        		}
        	}
            if (existeMaquina == true)
            {  
	            for (int i = 0; i < jsonArray.size(); i++) 
	            {
	            	JSONObject arrayObject = (JSONObject) jsonArray.get(i);
	            	String name = (String) arrayObject.get("nom");
	            	if (name.equals(nomAula))
	            	{
	            		JSONArray maquines = (JSONArray) arrayObject.get("maquines");
	            		maquines.add(JSONmaquina);
	            		
	            	}  
	            }
	            try (FileWriter file = new FileWriter("aules.json")) {
	
	                file.write(jsonArray.toJSONString());
	                file.flush();
	
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
            }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}	
		
	}
	
	public static void switchAC(String nomAula)
	{
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("aules.json"));
			JSONArray jsonArray = (JSONArray) obj;
			
            for (int i = 0; i < jsonArray.size(); i++) 
            {
            	JSONObject arrayObject = (JSONObject) jsonArray.get(i);
            	String name = (String) arrayObject.get("nom");
            	if (name.equals(nomAula))
            	{
                	Boolean aireacondicionat = (Boolean) arrayObject.get("aireacondicionat");
                	if (aireacondicionat == true)
                	{
                		arrayObject.put("aireacondicionat", false);
                	}
                	else
                	{
                		arrayObject.put("aireacondicionat", true);
                	}
            	}  
            	
            	
			}
            try (FileWriter file = new FileWriter("aules.json")) {

                file.write(jsonArray.toJSONString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
      
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public static void crearClasse()
	{
		Aula aula = new Aula();
		String professor = null;
		ArrayList<Alumne> alumnesSeleccionats = new ArrayList<Alumne>();
		
		ArrayList<String> professors = new ArrayList<String>();
		try {
			FileReader fr = new FileReader("professors.txt");
			BufferedReader br = new BufferedReader(fr);

			String line;
			while(br.ready()) {
				line = br.readLine();
				professors.add(line);
			}
			br.close();
			fr.close();
			Random rand = new Random(); 
			professor = professors.get(rand.nextInt(professors.size()));
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		try
		{
			File file = new File("alumnes.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Institut ins = (Institut) jaxbUnmarshaller.unmarshal(file);
		
			ArrayList<Alumne> alumnes = ins.getAlumnes();
			Collections.shuffle(alumnes);
			for (int i = 0; i < 5 && i < alumnes.size(); i++) 
			{
				alumnesSeleccionats.add(alumnes.get(i));
			}		
			
		} catch (JAXBException e)
		{
			e.printStackTrace();	
		}
		
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("aules.json"));
			JSONArray jsonArray = (JSONArray) obj;
			Collections.shuffle(jsonArray);
            for (int i = 0; i < 1 && i < jsonArray.size(); i++) 
            {
            	JSONObject arrayObject = (JSONObject) jsonArray.get(i);
            	aula.setNom((String) arrayObject.get("nom"));
            	aula.setAireacondicionat((Boolean) arrayObject.get("aireacondicionat"));
            	aula.setCapacitat((Long) arrayObject.get("capacitat"));
			
        		JSONArray maquines = (JSONArray) arrayObject.get("maquines");
				ArrayList<Maquina> maquinesAula = new ArrayList<Maquina>(arrayObject.size());
				for(Object s : maquines) 
        		{
					Maquina maquina = new Maquina();
					JSONObject maquinesObject = (JSONObject) s;
					maquina.setNom((String) maquinesObject.get("nom"));
					maquina.setProcessador((String) maquinesObject.get("processador"));
					maquina.setGrafica((Boolean) maquinesObject.get("grafica"));
					
					maquinesAula.add(maquina);
        		}
				aula.setMaquines(maquinesAula);
            }
      
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Classe classe = new Classe(aula, professor, alumnesSeleccionats);
		File file = new File("classes.dat");
		try
		{
			FileOutputStream fos = new FileOutputStream(file);
			
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(classe);
			
			oos.flush();
			
			fos.close();
			oos.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void llegirClasse()
	{
		ArrayList<Classe> classe = new ArrayList<Classe>();
		
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		
		File file = new File("classes.dat");

		try {
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);

			while (true) 
			{
				classe.add((Classe) ois.readObject());
			}
			
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			
			fis.close();
			ois.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for (Classe c : classe) {
			System.out.println(c);
		}
	}
}
