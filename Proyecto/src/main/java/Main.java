import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import objetos.Alumne;

public class Main {

	public static void main(String[] args) 
	{
		/*Si usas el menu en orden esta preparado para que se pueda ver que todo funciona.*/
		
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Pon una opcion del 1-10."
				+ "\n1.afegirProfessor"
				+"\n2.jubilarProfessor"
				+"\n3.afegeixAlumne"
				+"\n4.afegeixTelefon"
				+"\n5.alCarrer"
				+"\n6.comprarMaquina"
				+"\n7.canviaMaquina"
				+"\n8.switchAC"
				+"\n9.crearClasse"
				+"\n10.llegirClasse");
		int num = sc.nextInt();
			switch (num) {
			case 1:
				Metodos.afegirProfessor("Falso, Profesor");
				break;
			case 2:
				Metodos.jubilarProfessor("Falso, Profesor");
				break;
			case 3:
				Metodos.afegeixAlumne(new Alumne("Adrián", "Izquierdo Cano", "Calle Falsa, 123","25364346T",new ArrayList<String>(Arrays.asList(new String[] { "631845285", "572475864" })), "adrian@gmail.com"));
				break;
			case 4:
				Metodos.afegeixTelefon("25364346T", "999666333");
				break;
			case 5:
				Metodos.alCarrer("25364346T");
				break;
			case 6:
				Metodos.comprarMaquina("C4","3-4-5", "i7", true);
				break;
			case 7:
				Metodos.canviaMaquina("3-4-5", "1.6");
				break;
			case 8:
				Metodos.switchAC("1.6");
				break;
			case 9:
				Metodos.crearClasse();
				break;
			case 10:
				Metodos.llegirClasse();
				break;
			
		}
	}

}
