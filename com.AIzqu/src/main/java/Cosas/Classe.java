package Cosas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import Json.Aula;
import Xml.Alumne;

public class Classe implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3216508096832790270L;
	private Aula aula;
	private String professor;
	private ArrayList<Alumne> alumnes;

	private Classe(Aula aula, String professor, ArrayList<Alumne> alumnes) 
	{
		super();
		this.aula = aula;
		this.professor = professor;
		this.alumnes = alumnes;
	}

	public Aula getAula() {
		return aula;
	}

	public String getProfessor() {
		return professor;
	}

	public ArrayList<Alumne> getAlumnes() {
		return alumnes;
	}

	public String toString() 
	{
		String s = "aula: " + aula.getNom() + "professor: " + professor + "alumnes: ";

		for (Alumne alumn : alumnes) 
		{
			s += alumn.getCognoms() + ", " + alumn.getNom() + "; ";
		}
		return s;
	}

	public static Classe generarClase(ArrayList<Aula> aulas, ArrayList<String> profesores, ArrayList<Alumne> alumnos) 
	{
		Random ran = new Random();

		Aula aula = aulas.get(ran.nextInt(aulas.size()));
		String professor = profesores.get(ran.nextInt(profesores.size()));

		Collections.shuffle(alumnos);

		ArrayList<Alumne> alumnes = new ArrayList<Alumne>();

		for (int i = 0; i < 5 && i < alumnos.size(); i++) 
		{
			alumnes.add(alumnos.get(i));
		}

		return new Classe(aula, professor, alumnes);
	}

}
