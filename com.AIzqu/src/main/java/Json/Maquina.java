package Json;

import java.io.Serializable;

public class Maquina implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3599905418243781166L;	
	private String processador;
	private boolean grafica;
	private String nom;
	
	public Maquina() {}
	
	public Maquina( String processador, boolean grafica, String nom)
	{
		super();
		this.processador = processador;
		this.grafica = grafica;
		this.nom = nom;
	}
	

	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getProcessador() {
		return processador;
	}
	public void setProcessador(String processador) {
		this.processador = processador;
	}
	public boolean getGrafica() {
		return grafica;
	}
	public void setGrafica(boolean grafica) {
		this.grafica = grafica;
	}
	
	public String toString()
	{
		return processador+" "+(grafica ? "true":"false")+nom+" ";
	}
}