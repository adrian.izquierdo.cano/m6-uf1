package Json;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Aula2Json 
{

	@SuppressWarnings("unchecked")
	public void escribir(File file, ArrayList<Aula> aulas) 
	{
		JSONArray jsonAulas = new JSONArray();

		for (int i = 0; i < aulas.size(); i++) 
		{
			Aula aula = aulas.get(i);

			JSONObject jsonAula = new JSONObject();

			jsonAula.put("nom", aula.getNom());
			jsonAula.put("capacitat", aula.getCapacitat());
			jsonAula.put("aireacondicionat", aula.isAireacondicionat());

			ArrayList<Maquina> maquines = aula.getMaquines();
			JSONArray jsonMaquines = new JSONArray();

			for (int j = 0; j < maquines.size(); j++) 
			{
				Maquina maquina = maquines.get(j);
				JSONObject jsonMaquina = new JSONObject();

				jsonMaquina.put("processador", maquina.getProcessador());
				jsonMaquina.put("grafica", maquina.getGrafica());
				jsonMaquina.put("nom", maquina.getNom());

				jsonMaquines.add(jsonMaquina);
			}

			jsonAula.put("maquines", jsonMaquines);

			jsonAulas.add(jsonAula);
		}

		try {
			FileWriter fw = new FileWriter(file);

			BufferedWriter bw = new BufferedWriter(fw);

			bw.write(jsonAulas.toJSONString());

			bw.flush();

			fw.close();

			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void escribir(String path, ArrayList<Aula> aulas) {
		escribir(new File(path), aulas);
	}
}
