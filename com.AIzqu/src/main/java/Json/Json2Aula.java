package Json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Json2Aula 
{
	public ArrayList<Aula> leer(File file) 
	{
		ArrayList<Aula> aulas = new ArrayList<Aula>();

		FileReader fr = null;

		try {
			fr = new FileReader(file);

			JSONParser parser = new JSONParser();

			JSONArray jsonAulas = (JSONArray) parser.parse(fr);

			for (int i = 0; i < jsonAulas.size(); i++) 
			{
				Aula aula = new Aula();

				JSONObject object = (JSONObject) jsonAulas.get(i);

				aula.setNom((String) object.get("nom"));
				aula.setAireacondicionat((Boolean) object.get("aireacondicionat"));
				aula.setCapacitat((Long) object.get("capacitat"));

				JSONArray jsonMaquines = (JSONArray) object.get("maquines");

				ArrayList<Maquina> maquines = new ArrayList<Maquina>(jsonMaquines.size());

				for (int j = 0; j < jsonMaquines.size(); j++) {
					Maquina maquina = new Maquina();

					JSONObject jsonMaquina = (JSONObject) jsonMaquines.get(j);

					maquina.setNom((String) jsonMaquina.get("nom"));
					maquina.setProcessador((String) jsonMaquina.get("processador"));
					maquina.setGrafica((Boolean) jsonMaquina.get("grafica"));

					maquines.add(maquina);
				}

				aula.setMaquines(maquines);
				aulas.add(aula);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return aulas;
	}

	public ArrayList<Aula> leer(String path) {
		return leer(new File(path));
	}
}
