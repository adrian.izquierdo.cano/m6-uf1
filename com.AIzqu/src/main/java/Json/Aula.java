package Json;

import java.io.Serializable;
import java.util.ArrayList;

public class Aula implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3235690749547003801L;
	private String nom;
	private long capacitat;
	private boolean aireacondicionat;
	private ArrayList<Maquina> maquines;

	public String getNom() {
		return nom;
	}

	public long getCapacitat() {
		return capacitat;
	}

	public void setCapacitat(long capacitat) {
		this.capacitat = capacitat;
	}

	public boolean isAireacondicionat() {
		return aireacondicionat;
	}

	public void setAireacondicionat(boolean aireacondicionat) {
		this.aireacondicionat = aireacondicionat;
	}

	public ArrayList<Maquina> getMaquines() {
		return maquines;
	}

	public void setMaquines(ArrayList<Maquina> maquines) {
		this.maquines = maquines;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}