package Serializacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Serializador 
{

	public <E extends Serializable> void escribir(File file, E object)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(file);
			
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(object);
			
			oos.flush();
			
			fos.close();
			oos.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public <E extends Serializable> void appendar(File file, E object)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(file, true);
			
			ObjectOutputStream oos = new ObjectOutputStream(fos)
			{
				@Override
				protected void writeStreamHeader() throws IOException
				{
					reset();
				}
			};
			
			oos.writeObject(object);
			
			oos.flush();
			
			fos.close();
			oos.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
