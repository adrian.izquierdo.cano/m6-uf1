package Serializacion;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class Deserializar 
{

	public Object leer(File file) 
	{	
		Object object = null;

		try {
			FileInputStream fis = new FileInputStream(file);

			ObjectInputStream ois = new ObjectInputStream(fis);

			object = ois.readObject();

			fis.close();
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return object;
	}

	@SuppressWarnings("unchecked")
	public static <E> ArrayList<E> leerLista(File file) {
		ArrayList<E> objects = new ArrayList<E>();

		FileInputStream fis = null;
		ObjectInputStream ois = null;

		try {
			fis = new FileInputStream(file);

			ois = new ObjectInputStream(fis);

			while (true) {
				objects.add((E) ois.readObject());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			fis.close();
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return objects;
	}

	public Object leer(String path) {
		return leer(new File(path));
	}
	
	public static <E> ArrayList<E> leerLista(String path) 
	{
		return leerLista(new File(path));
	}
}
