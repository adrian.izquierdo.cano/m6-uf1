package Ejercicio;

import java.util.ArrayList;
import java.util.Arrays;

import Xml.Alumne;

public class Main {

	public static void main(String[] args) {
		Metodo.afegirProfessor("Ventas, Sr");

		Metodo.jubilarProfessor("Albareda, David Marc");

		Metodo.afegirAlumne(new Alumne("Adrian", "Izquierdo Cano", "25364346T", "Calle Falsa, 123",
				new ArrayList<String>(Arrays.asList(new String[] { "625724562", "874317423" })), "D:@:L.com"));

		Metodo.afegeixTelefon("25364346T", "555333222");

		Metodo.alCarrer("28923792Z");

		Metodo.comprarMaquina("C4", "3-4-5", false, "i3");

		Metodo.canviaMaquina("3-4-4", "1.6");

		Metodo.switchAC("C4");

		Metodo.crearClasse();

		Metodo.llegirClasses();

	}
}
