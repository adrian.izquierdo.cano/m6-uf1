package Ejercicio;

import java.util.ArrayList;
import java.io.File;
import java.util.Collections;

import Cosas.Classe;
import Json.Aula;
import Json.Aula2Json;
import Json.Json2Aula;
import Json.Maquina;
import Serializacion.Deserializar;
import Serializacion.Serializador;
import Xml.Alumne;
import Xml.Institut;
import Xml.Institut2Xml;
import Xml.Xml2Institut;
import txt.Escribirtxt;
import txt.Leertxt;

public class Metodo {
	public static void afegirProfessor(String nom) {
		Leertxt ltxt = new Leertxt();

		ArrayList<String> professors = ltxt.leer("professors.txt");

		professors.add(nom);

		Collections.sort(professors);

		Escribirtxt etxt = new Escribirtxt();

		etxt.escribir("professors.txt", professors);
	}

	public static void jubilarProfessor(String nom) {
		Leertxt ltxt = new Leertxt();

		ArrayList<String> professors = ltxt.leer("professors.txt");

		professors.remove(nom);

		Collections.sort(professors);

		Escribirtxt etxt = new Escribirtxt();

		etxt.escribir("professors.txt", professors);
	}

	public static void afegirAlumne(Alumne al) {
		Xml2Institut xml2insti = new Xml2Institut();

		Institut institut = xml2insti.leer("alumnes.xml");

		institut.getAlumnes().add(al);

		Institut2Xml insti2xml = new Institut2Xml();

		insti2xml.escribir("alumnes.xml", institut);
	}

	public static void afegeixTelefon(String dni, String telefon) {
		Xml2Institut xml2insti = new Xml2Institut();

		Institut institut = xml2insti.leer("alumnes.xml");

		ArrayList<Alumne> alumnes = institut.getAlumnes();

		for (int i = 0; i < alumnes.size(); i++) {
			Alumne a = alumnes.get(i);

			if (a.getDni().equals(dni)) {
				a.getTelefons().add(telefon);
				break;
			}
		}

		Institut2Xml insti2xml = new Institut2Xml();

		insti2xml.escribir("alumnes.xml", institut);
	}

	public static void alCarrer(String dni) {
		Xml2Institut xml2insti = new Xml2Institut();
		Institut institut = xml2insti.leer("alumnes.xml");
		ArrayList<Alumne> alumnes = institut.getAlumnes();

		for (int i = 0; i < alumnes.size(); i++) {
			Alumne a = alumnes.get(i);

			if (a.getDni().equals(dni)) {
				alumnes.remove(i);
				break;
			}
		}

		Institut2Xml insti2xml = new Institut2Xml();
		insti2xml.escribir("alumnes.xml", institut);
	}

	public static void comprarMaquina(String nomAula, String processador, boolean grafica, String nomMaquina) {
		Maquina maquina = new Maquina(processador, grafica, nomMaquina);

		Json2Aula jota2aul = new Json2Aula();

		ArrayList<Aula> aules = jota2aul.leer("aules.json");

		for (int i = 0; i < aules.size(); i++) {
			Aula a = aules.get(i);

			if (a.getNom().equals(nomAula)) {
				a.getMaquines().add(maquina);

				break;
			}
		}

		Aula2Json aul2jota = new Aula2Json();

		aul2jota.escribir("aules.json", aules);
	}

	public static void canviaMaquina(String nomMaquina, String nomAula) {
		Json2Aula jota2aul = new Json2Aula();
		ArrayList<Aula> aules = jota2aul.leer("aules.json");
		Maquina maquina = null;

		for (int i = 0; i < aules.size(); i++) {
			ArrayList<Maquina> maquines = aules.get(i).getMaquines();

			for (int k = 0; k < maquines.size(); k++) {
				Maquina maq = maquines.get(k);

				if (maq.getNom().equals(nomMaquina)) {
					maquina = maq;

					maquines.remove(maq);

					break;
				}
			}
		}

		for (int i = 0; i < aules.size(); i++) {
			Aula a = aules.get(i);

			if (a.getNom().equals(nomAula) && maquina != null) {
				a.getMaquines().add(maquina);

				break;
			}
		}

		Aula2Json aul2jota = new Aula2Json();

		aul2jota.escribir("aules.json", aules);
	}

	public static void switchAC(String nomAula) {
		Json2Aula jota2aul = new Json2Aula();

		ArrayList<Aula> aules = jota2aul.leer("aules.json");

		for (int i = 0; i < aules.size(); i++) {
			Aula a = aules.get(i);

			if (a.getNom().equals(nomAula)) {
				a.setAireacondicionat(!a.isAireacondicionat());
				;
				break;
			}
		}

		Aula2Json aul2jota = new Aula2Json();

		aul2jota.escribir("aules.json", aules);
	}

	public static void crearClasse() {
		Leertxt ltxt = new Leertxt();

		ArrayList<String> professors = ltxt.leer("professors.txt");

		Json2Aula jota2aul = new Json2Aula();

		ArrayList<Aula> aules = jota2aul.leer("aules.json");

		Xml2Institut xml2insti = new Xml2Institut();

		Institut institut = xml2insti.leer("alumnes.xml");

		Classe classe = Classe.generarClase(aules, professors, institut.getAlumnes());

		File file = new File("classes.dat");

		Serializador serial = new Serializador();

		if (!file.exists()) {
			serial.escribir(file, classe);
		} else {
			serial.appendar(file, classe);
		}
	}

	public static void llegirClasses() {
		Deserializar des = new Deserializar();

		ArrayList<Classe> classes = Deserializar.<Classe>leerLista("classes.dat");

		for (Classe classe : classes) {
			System.out.println(classe);
		}

	}

}
