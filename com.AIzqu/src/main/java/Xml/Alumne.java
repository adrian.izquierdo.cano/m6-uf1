package Xml;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="alumne")
@XmlType(propOrder={"nom", "cognoms" , "dni", "adreca", "telefons", "mail" })

public class Alumne implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8264097840227125728L;
	private String nom;
	private String cognoms;
	private String DNI;
	private String adreca;
	private ArrayList<String> telefons = new ArrayList<String>();
	private String mail;
	
	public Alumne()
	{
		
	}
	
	public Alumne(String nom, String cognoms, String DNI, String adreca, ArrayList<String> telefons, String mail) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.DNI = DNI;
		this.adreca = adreca;
		this.telefons = telefons;
		this.mail = mail;
	}
	

	@XmlElement(name="nom")
	public String getNom()
	{
		return nom;
	}
	
	public void setNom(String nom)
	{
		this.nom = nom;
	}
	
	@XmlElement(name="cognoms")
	public String getCognoms()
	{
		return cognoms;
	}
	
	public void setCognoms(String cognoms)
	{
		this.cognoms = cognoms;
	}
	
	@XmlElement(name="DNI")
	public String getDni()
	{
		return DNI;
	}
	
	public void setDni(String dni)
	{
		this.DNI = dni;
	}
	
	@XmlElement(name="adreca")
	public String getAdreca()
	{
		return adreca;
	}
	
	public void setAdreca(String adreca)
	{
		this.adreca = adreca;
	}
	
	@XmlElementWrapper(name="telefons")
	@XmlElement(name="telefon")
	public ArrayList<String> getTelefons()
	{
		return telefons;
	}
	
	public void setTelefons(ArrayList<String> telefons)
	{
		this.telefons = telefons;
	}
	
	@XmlElement(name="mail")
	public String getMail()
	{
		return mail;
	}

	public void setMail(String mail)
	{
		this.mail = mail;
	}
	
	public static Alumne alumnoRandom()
	{
		String letras = "qwertyuioplkjhgfdsazxcvbnm";
		
		return null;
	}
}
