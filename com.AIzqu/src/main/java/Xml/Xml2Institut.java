package Xml;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class Xml2Institut {
	public Institut leer(File file) 
	{
		Institut institut = null;

		try {
			JAXBContext context = JAXBContext.newInstance(Institut.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			institut = (Institut) unmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return institut;
	}

	public Institut leer(String path) {
		return leer(new File(path));
	}
}