package Xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class Institut2Xml {
	public void escribir(File file, Institut institut) 
	{
		FileOutputStream fos = null;

		try {
			JAXBContext context = JAXBContext.newInstance(Institut.class);
			Marshaller marshaller = context.createMarshaller();

			fos = new FileOutputStream(file);

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			marshaller.marshal(institut, fos);
		} catch (JAXBException e) {

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void escribir(String path, Institut institut) {
		escribir(new File(path), institut);
	}
}
